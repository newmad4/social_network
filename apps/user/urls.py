from django.urls import path

from user.views import CustomTokenObtainPairView, UserLastActivityAPIView

app_name = 'user'

urlpatterns = [
    path('jwt/create/', CustomTokenObtainPairView.as_view(), name='jwt-create'),
    path('last-activity/', UserLastActivityAPIView.as_view(), name='last-activity')
]
