from django.contrib.auth.models import update_last_login
from django.urls import reverse
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from user.utils import save_last_activity


__all__ = [
    'CustomTokenObtainPairSerializer'
]


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    """
    Serializer for process return token pairs
    """
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        update_last_login(None, user)  # Update user last login
        save_last_activity(
            user=user, request_method="POST", response_status_code=200, url_path=reverse('user:jwt-create')
        )  # Save user last activity with obtain token

        return token
