from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import ugettext as _

from user.managers import UserManager

__all__ = [
    "User",
    "LastActivity"
]


class User(AbstractBaseUser, PermissionsMixin):
    date_joined = models.DateTimeField(auto_now_add=True)
    last_activity_time = models.DateTimeField(auto_now=True)
    username = models.CharField(max_length=30, null=True, help_text=_("User name for interaction with other users"))
    email = models.EmailField(unique=True, help_text=_("Main user field in system"))

    # Service Fields
    is_staff = models.BooleanField(
        default=False, help_text=_("Designates whether the user can log into this admin site"),
    )
    is_active = models.BooleanField(
        default=True, help_text=_(
            "Designates whether this user should be treated as active. Unselect this instead of deleting accounts"
        ),
    )
    objects = UserManager()
    USERNAME_FIELD = "email"

    def __str__(self):
        return self.username or self.email.split('@')[0]


class LastActivity(models.Model):
    """
    Model for save last user activity
    """
    dt_updated = models.DateTimeField(auto_now=True, help_text=_("Last datetime user activities"))
    user = models.OneToOneField(
        User,
        related_name="last_activity",
        help_text="User for whom this record",
        on_delete=models.CASCADE
    )
    ip_address = models.GenericIPAddressField(help_text=_("User last point ip address"), null=True)
    url_path = models.CharField(
        max_length=256, help_text=_("User last request destination path with query params"), null=True
    )
    method = models.CharField(max_length=15, help_text=_("User last request method"), null=True)
    status_code = models.SmallIntegerField(help_text=_("Response status code from last request"), null=True)

    class Meta:
        db_table = "user_last_activity"
