from rest_framework import status
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from user.serializers import CustomTokenObtainPairSerializer


__all__ = [
    'CustomTokenObtainPairView',
    'UserLastActivityAPIView'
]


class CustomTokenObtainPairView(TokenObtainPairView):
    """
    View which return access and refresh tokens
    """
    serializer_class = CustomTokenObtainPairSerializer


class UserLastActivityAPIView(RetrieveAPIView):
    """
    View which return last user login and last activity time
    """
    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):

        return Response(
            data={
                "last_login": request.user.last_login,
                "last_activity": request.user.last_activity.dt_updated if request.user.last_activity else None
            },
            status=status.HTTP_200_OK
        )
