from django.core.handlers.wsgi import WSGIRequest
from django.urls import reverse
from django.utils.deprecation import MiddlewareMixin
from rest_framework.response import Response

from user.utils import save_last_activity


__all__ = [
    'SaveLastActivityMiddleware'
]


class SaveLastActivityMiddleware(MiddlewareMixin):
    """
    Middleware for check in every user activities without
    """
    def process_request(self, request: WSGIRequest) -> Response:
        response = self.get_response(request)
        if not (
                request.user.is_anonymous or
                request.META.get("PATH_INFO") == reverse('user:last-activity')
        ):
            save_last_activity(
                user=request.user,
                request_meta=request.META,
                request_method=request.method,
                response_status_code=response.status_code
            )
        return response
