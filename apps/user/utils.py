from django.contrib.auth.models import User
from django.utils import timezone

from user.models import LastActivity


__all__ = [
    'save_last_activity'
]


def save_last_activity(
        user: User, request_meta: dict = None, request_method: str = None, response_status_code: int = None, **kwargs
) -> None:
    """
    Function for store user last activity
    """
    ip = ""
    url_path = ""
    if request_meta:
        x_forwarded_for = request_meta.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[-1].strip()
        else:
            ip = request_meta.get('REMOTE_ADDR')

        url_path = request_meta.get("PATH_INFO") + request_meta.get("QUERY_STRING", "")

    user.last_activity_time = timezone.now()
    user.save()

    user_activity, created = LastActivity.objects.get_or_create(user=user)
    user_activity.ip_address = ip
    user_activity.url_path = url_path or kwargs.get("url_path")
    user_activity.method = request_method
    user_activity.status_code = response_status_code
    user_activity.save()
