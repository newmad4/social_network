from django.db import models
from django.utils.translation import ugettext as _

from user.models import User


__all__ = [
    'Post',
    'Like'
]


class Post(models.Model):
    """
    Model for post in system
    """
    user = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE, help_text=_("Author of post"))
    title = models.CharField(max_length=256, help_text=_("Title of post"))
    text = models.TextField(help_text=_("Text of post"))
    dt_created = models.DateTimeField(auto_now_add=True, help_text=_("Date and time when post created"))
    dt_updated = models.DateTimeField(auto_now=True, help_text=_("Date and time when post updated"))


class Like(models.Model):
    """
    Model for store user likes and unlikes for post
    """
    dt_created = models.DateTimeField(auto_now_add=True, help_text=_("Date and time when likes added"), db_index=True)
    LIKE = 1
    UNLIKE = -1
    LIKE_TYPES = (
        (LIKE, 'Like'),
        (UNLIKE, 'Unlike')
    )
    like_type = models.SmallIntegerField(choices=LIKE_TYPES, null=True, blank=True, help_text=_("Like or Unlike?"))
    user = models.ForeignKey(User, on_delete=models.CASCADE, help_text=_("Who liked post"))
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, help_text=_("Link to post which was liked or unliked")
    )

    class Meta:
        default_related_name = 'likes'
