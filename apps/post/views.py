import datetime

from rest_framework import status
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.response import Response

from post.models import Post, Like
from post.serializers import PostSerializer, LikeUnlikeSerializer
from post.utils import get_total_likes_aggregated_by_day

__all__ = [
    'PostCreateAPIView',
    'LikeUnlikeCreateAPIView',
    'LikeAnalyticsAPIView'
]


class PostCreateAPIView(CreateAPIView):
    """
    API view for create post
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class LikeUnlikeCreateAPIView(CreateAPIView):
    """
    API view for add like or unlike for post
    """
    permission_classes = (IsAuthenticated, )
    serializer_class = LikeUnlikeSerializer
    queryset = Like.objects.all()


class LikeAnalyticsAPIView(RetrieveAPIView):
    """
    API view for get like analytics for current user
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        # get query params
        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')

        if not (date_from or date_to):
            return Response(data=get_total_likes_aggregated_by_day(request.user), status=status.HTTP_200_OK)

        try:
            date_from = datetime.date.fromisoformat(date_from)
            date_to = datetime.date.fromisoformat(date_to)
        except (ValueError, TypeError):
            return Response(
                data="Provided data is not in ISO date format", status=status.HTTP_400_BAD_REQUEST
            )
        if date_from > date_to:
            return Response(
                data="Date from must occur after date to", status=status.HTTP_400_BAD_REQUEST
            )

        return Response(
            data=get_total_likes_aggregated_by_day(request.user, date_from=date_from, date_to=date_to),
            status=status.HTTP_200_OK
        )
