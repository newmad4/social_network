from datetime import date

from django.db.models import Count
from django.db.models.query import QuerySet

from post.models import Like
from user.models import User


__all__ = [
    'get_total_likes_aggregated_by_day'
]


def get_total_likes_aggregated_by_day(
        user: User, date_from: date = None, date_to: date = None, like_type: int = Like.LIKE
) -> QuerySet:
    """
    Return total likes or unlikes aggregated by day from date range for chosen user
    """
    likes_by_date_qs = Like.objects.filter(user=user, like_type=like_type)
    if date_from and date_to:
        likes_by_date_qs = likes_by_date_qs.filter(dt_created__date__range=[date_from, date_to])

    return likes_by_date_qs.\
        extra(select={'date_created': "date(dt_created)"}).\
        values('date_created').\
        annotate(likes_count=Count('id'))
