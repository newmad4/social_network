from django.urls import path
from apps.post.views import PostCreateAPIView, LikeUnlikeCreateAPIView, LikeAnalyticsAPIView

app_name = 'post'

urlpatterns = [
    path('', PostCreateAPIView.as_view(), name='post-create'),
    path('like/', LikeUnlikeCreateAPIView.as_view(), name='post-like'),
    path('like/analytics/', LikeAnalyticsAPIView.as_view(), name='like-analytics'),
]
