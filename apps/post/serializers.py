from rest_framework import serializers
from post.models import Post, Like

__all__ = [
    'PostSerializer',
    'LikeUnlikeSerializer'
]


class PostSerializer(serializers.ModelSerializer):
    """
    Serializer for create post
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'user', 'dt_created', 'dt_updated')


class LikeUnlikeSerializer(serializers.ModelSerializer):
    """
    Serializer for create like or unlike
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    def create(self, validated_data):
        like, _ = Like.objects.get_or_create(post=validated_data['post'], user=validated_data['user'])

        if like.like_type != validated_data['like_type']:
            like.like_type = validated_data['like_type']
            like.save()

        return like

    class Meta:
        model = Like
        fields = ['id', 'dt_created', 'user', 'like_type', 'post']
